package br.filipe.modelo.negocio.dominio.entidades;

import br.filipe.modelo.negocio.dominio.anotacoes.*;

public class Usuario {

	@ValidarPresencaInterface()
	private String nome;

	@ValidarPresencaInterface(mensagem="preencha o email")
	private String email;

	private Integer idade;

	
	public Usuario(String nome, String email, Integer idade) {
		super();
		this.nome = nome;
		this.email = email;
		this.idade = idade;
		this.validar();
	}
	
	private void validar() {
		ValidarPresenca.executar(ValidarPresencaInterface.class, this);
	}

	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

}
