package br.filipe.modelo.negocio.dominio.interfaces;

import br.filipe.modelo.negocio.dominio.entidades.Usuario;

public interface FachadaInterface {

	void listaUsuarios();
	void salvaUsuario(Usuario u);
	void excluiUsuario(Usuario u);
}
