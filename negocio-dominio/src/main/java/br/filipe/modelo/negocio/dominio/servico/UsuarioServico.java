package br.filipe.modelo.negocio.dominio.servico;

import br.filipe.modelo.negocio.dominio.entidades.Usuario;
import br.filipe.modelo.negocio.dominio.interfaces.UsuarioServicoInterface;

public class UsuarioServico implements UsuarioServicoInterface {

	public boolean eMaiorDe18anos(Usuario usuario) {
		if(usuario.getIdade() > 18) {
			return true;
		} else {
			return false;
		}
	}
}
