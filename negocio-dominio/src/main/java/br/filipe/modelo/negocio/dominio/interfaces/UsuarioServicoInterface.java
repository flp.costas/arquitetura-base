package br.filipe.modelo.negocio.dominio.interfaces;

import br.filipe.modelo.negocio.dominio.entidades.Usuario;

public interface UsuarioServicoInterface {
	public boolean eMaiorDe18anos(Usuario usuario);
}
