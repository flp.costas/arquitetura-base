package br.filipe.modelo.negocio.dominio.excecoes;

@SuppressWarnings("serial")
public class ValidarPresencaExcecao extends RuntimeException {

	public ValidarPresencaExcecao(String mensagem) {
		super(mensagem);
	}
}
