package br.filipe.modelo.negocio.dominio.bean;

import java.util.Collection;

import br.filipe.modelo.dados.repositorio.interfaces.UsuarioRepositorioInterface;
import br.filipe.modelo.negocio.dominio.entidades.Usuario;

public class UsuarioBean {
	Usuario usuario;
	UsuarioRepositorioInterface repositorio;
	
	public UsuarioBean(Usuario usuario, UsuarioRepositorioInterface repositorio) {
		this.usuario = usuario;
		this.repositorio = repositorio;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Usuario> listar() {
		return (Collection<Usuario>) repositorio.lista();
	}
	
	public void salvar() {
		repositorio.salva(usuario);
	}
	
	public void excluir() {
		repositorio.exclui(usuario);
	}
	
	public Usuario retornaPorEmail(String email) {
		return (Usuario) repositorio.retornaPorEmail(email);
	}
	
}
