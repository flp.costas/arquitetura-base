package br.filipe.modelo.dados.repositorio.interfaces;

import java.util.Collection;

public interface RepositorioInterface<T> {

	Collection<?> lista();
	void salva(T o);
	void exclui(T o);
}
