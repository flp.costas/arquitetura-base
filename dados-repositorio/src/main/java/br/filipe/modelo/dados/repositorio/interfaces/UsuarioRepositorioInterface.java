package br.filipe.modelo.dados.repositorio.interfaces;

public interface UsuarioRepositorioInterface extends RepositorioInterface<Object> {
	Object retornaPorEmail(String email);
}
